<?php
namespace controllers;

use base\BaseView as View;
use base\BaseController;
use models\News;

/**
 * Class NewsController
 * @package controllers
 */
class NewsController extends BaseController
{
    public function actionIndex()
    {
        $view = new View('news\indexTpl');
        $news = new News();
        echo $view->render(array('news'=>$news->getAllNews()));
    }

    public function actionFeed($data){
        $id = str_replace('id=', '', $data);
        if(intval($id)){
            $news = new News();
            $newsData =  $news->getNewsById($id);
            if($newsData){
                $view = new View('news\feedTpl');
                echo $view->render(array('news'=>$newsData));
                return true;
            }
            echo 'News not found';
            return false;

        }
        echo 'Incorrect url 404';
        return false;
    }
}