<?php
namespace controllers;

use base\BaseView as View;
use base\BaseController;
use models\News;
use models\User;

/**
 * Class UserController
 * @package controllers
 * CRUD actions for news, login/logout
 */
class UserController extends BaseController
{
    /**
     * @var \base\Authorisation
     */
    private $authorisation;

    public function __construct()
    {
        $this->authorisation = new \base\Authorisation();
    }

    public function actionLogin()
    {
        if($this->authorisation->isAuthorised()){
            header('Location: '.'http://'.$_SERVER['SERVER_NAME'].'/user');
        }

        if(isset($_POST['login']) && isset($_POST['password'])){
            $userModel = new \models\User();
            $this->authorisation->login(
                $_POST['login'],
                $_POST['password'],
                $userModel
            );
            if(intval($userModel->user['id'])){
                echo 1;
                return true;
            }
            return false;
        }else{
            $view = new View('user\loginTpl');
            echo $view->render();
        }
    }



    public function actionIndex()
    {
        if($this->authorisation->isAuthorised()){
            $news = new News();
            $userNews = $news->getNewsByUId($_SESSION['user']['id']);
            $view = new View('user\indexTpl');
            echo $view->render(array('userData'=>$userNews));
        }else{
            header('Location: user/login');
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function actionCreateNews()
    {
        if($this->authorisation->isAuthorised()){
            $news = new News();
            $data = $_POST['News'];
            $data['author_id'] = $_SESSION['user']['id'];

            if($news->save($data)){
                echo 'News is successfully added';
                return true;
            }
            throw new \Exception('Can`t save this news');
        }
        throw new \Exception('Permission denide');
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function actionUpdateNews()
    {
        if($this->authorisation->isAuthorised()){
            $news = new News();
            $data = $_POST['News'];
            $data['author_id'] = $_SESSION['user']['id'];

            if($news->update($data)){
                echo 'News is successfully updated';
                return true;
            }
            throw new \Exception('Can`t update this news');
        }
        throw new \Exception('Permission denide');
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function actionDeleteNews(){
        if($this->authorisation->isAuthorised()){
            $news = new News();
            if(
                $news->deleteByUser($_POST['id'], $_SESSION['user']['id'])
            ){
                echo 'News is successfully deleted';
                return true;
            }
            throw new \Exception('Can`t deleted this news');
        }
        throw new \Exception('Permission denide');
    }

    public function actionGetNewsById(){
        $id = $_POST['id'];
        $news = new News();
        echo json_encode($news->getNewsById($id, $_SESSION['user']['id']));
    }

    public function actionExit(){
        if($this->authorisation->isAuthorised()){
            return $this->authorisation->logout();
        }
        throw new \Exception('Permission denide');
    }

}