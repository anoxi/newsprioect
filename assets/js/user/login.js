$(function () {
    $('form[name="loginForm"]').on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            type: 'post',
            url: '//'+window.location.host + window.location.pathname,
            data: {'login':$('#login').val(), 'password': $('#password').val()},
            success: function(data){
                if(data == '1'){
                    location.reload('/user');
                }else{
                    alert('User Not Found');
                }

            },
            fail: function () {
                alert('Can`t login');
            }
        });
        return false;
    });
});
