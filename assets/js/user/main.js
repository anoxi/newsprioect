$(function () {
    /*
    * set modal action
    * */
    $('#InfoModal').modal({
            complete: function() {
                location.reload();
            }
        }
    );
    $('#update').modal();

    /**
     * mobile menu events
     */
    $('.button-collapse').sideNav();

    /*
    * add message and show minfo message in modal
    * */
    function setInfoModal(text) {
        $('#InfoModal').find('#customModalInfo').text(text);
        $('#InfoModal').modal('open');
    }

    /*
    * logout
    * */
    $('#exit').on('click', function () {
        $.ajax({
            type: 'post',
            url: 'user/exit',
            data: '',
            success: function(){
                location.reload();
            },
            fail: function () {
                setInfoModal('Can`t send request. Please, try again.');
            }
        });
    });

    /*
    * send data for create or update news
    * */
    $('form[name="createNews"], form[name="updateNews"]').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);

        $.ajax({
            type: 'post',
            url: form.attr('action'),
            data: form.serialize(),
            success: function(data){
                setInfoModal(data);
                form.trigger("reset");
            },
            fail: function () {
                setInfoModal('Can`t send request. Please, try again.');
            }
        });
    });


    /*
    * click events on update and create btn
    * */
    $('.newsControlBtn button').on('click', function (e) {
        e.preventDefault();

        var action = $(this).attr('data-action');
        var newsId = $(this).parent().attr('data-news-id');

        if(action.indexOf('delete') !== -1){
            if(confirm('Are you sure to delete this record?')){
                $.ajax({
                    type: 'post',
                    url: action,
                    data: {id:newsId},
                    success: function(data){
                        setInfoModal(data);
                    },
                    fail: function () {
                        setInfoModal('Can`t send request. Please, try again.');
                    }
                });
            }
        }else if(action.indexOf('getNewsById') !== -1){
            $.ajax({
                type: 'post',
                url: action,
                data: {id:newsId},
                success: function(data){
                    var dataObj =  JSON.parse(data);
                    var form =  $('form[name="updateNews"]');
                    /**
                     * add class active all labels
                     */
                    form.find('label').each(function(){
                        $( this ).addClass( "active" );
                    });
                    /**
                     * set data to form field
                     */
                    form.find('#updateId').val(dataObj.id);
                    form.find('#updateTitle').val(dataObj.title);
                    form.find('#updateDescription').val(dataObj.description);
                    form.find('#updateText').val(dataObj.text);
                    $('#update').modal('open');
                },
                fail: function () {
                    setInfoModal('Can`t send request. Please, try again.');
                }
            });
        }

    });
});