-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 01 2017 г., 17:19
-- Версия сервера: 5.5.53
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `anoxi_news`
--

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `author_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `text`, `author_id`, `create_time`) VALUES
(3, 'В Україні 1 березня знову подорожчала електроенергія', 'В Україні з 1 березня 2017 року піднялися тарифи на електричну енергію відповідно до прийнятого раніше плану підвищення тарифів НКРЭКУ до рівня 90 коп/кВт-год за споживання до 100', 'В Україні з 1 березня 2017 року піднялися тарифи на електричну енергію відповідно до прийнятого раніше плану підвищення тарифів НКРЭКУ до рівня 90 коп/кВт-год за споживання до 100 кВт-год на місяць і 1,68 грн/кВт-год за споживання понад 100 кВт-год на місяць.\r\nПро це свідчить постанова НКРЕКП, прийнята в кінці лютого 2015 року, пише УНІАН.\r\n\"Це п\'яте заплановане підвищення тарифів на електроенергію для населення\", - йдеться у повідомленні.\r\nПорівняно з квітнем 2015 року тариф на електроенергію зріс у 3,5 разу.\r\nЯк повідомлялося, у лютому 2015 року НКРЭКУ затвердила дворічний графік підвищення тарифів на електроенергію для населення.\r\nЗгідно з цим графіком тарифи були поетапно підняті з 1 квітня та 1 вересня 2015 року в середньому на 45,2% і 23% відповідно.\r\nЗ 1 березня 2016 року НКРЕКП підвищила тариф на електроенергію для населення до 57 коп. за кВт-год за споживання до 100 кВт-ч.', 1, '2017-03-01 13:21:11'),
(4, 'Кутовий: Купівлю землі потрібно дозволити виключно українцям', 'Обіг земель має бути інструментом виключно для громадян України без права користування ним юридичними особами та іноземцями.\r\nПро це сказав міністр аграрної політики та продовольства Тарас Кутовий, повідомляє прес-служба.', 'Обіг земель має бути інструментом виключно для громадян України без права користування ним юридичними особами та іноземцями.\r\nПро це сказав міністр аграрної політики та продовольства Тарас Кутовий, повідомляє прес-служба.\r\n\"Наразі ми маємо дві чіткі тези: можливість купувати пайову землю має бути тільки для українських громадян і тільки для фізичних осіб. Обсяг володіння повинен бути обмежений, а ціна – не нижче за нормативно-правову оцінку\", - зазначив Кутовий.\r\nЗа словами міністра, суттєва частина проблем, які сьогодні існують у земельній реформі, полягає у тому, що обіг земель не врегульований.\r\n\"Фермер володіє та користується землею, але, на жаль, не може нею розпоряджатися. Якщо фермер сьогодні власник, він повинен бути власником і у дійсності. Тому що це актив, над яким він сьогодні працює і хоче залишити у спадок своїм нащадкам\", - наголосив очільник аграрного міністерства.\r\nКутовий зауважив, що рішення, які стосуються земельної реформи, будуть прийматися у стінах Верховної Ради.\r\nТакож міністр висловив впевненість у необхідності збільшення державної підтримки для фермерів.\r\n\"Вважаю, що підтримку на фермерські господарства необхідно збільшити більше ніж у 20 разів. Якщо у цьому році ми маємо менше 100 млн, то на наступний рік - вважаю мінімум 2 млрд з тієї великої дотації, яка вже затверджена бюджетним кодексом\", - поінформував Кутовий.\r\nЯк повідомлялося, для продовження співпраці з Міжнародним валютним фондом Україні потрібно провести пенсійну реформу, скоротити кількість держслужбовців і бюджетників, скасувати \"спрощенку\" і відкрити земельний ринок. \r\nЧитайте також: Ринок землі, пенсійна реформа, скасування \"спрощенки\". Чого чекає МВФ від України в 2017.', 1, '2017-03-01 13:22:02'),
(2, 'Суддю Чауса затримали в Молдові', 'Правоохоронці затримали підозрюваного у хабарництві суддю Дніпровського райсуду Києва Миколу Чауса в Молдові', 'Суддю Чауса затримали в Молдові\', \'Правоохоронці затримали підозрюваного у хабарництві суддю Дніпровського райсуду Києва Миколу Чауса в Молдові.\', \'Правоохоронці затримали підозрюваного у хабарництві суддю Дніпровського райсуду Києва Миколу Чауса в Молдові.\\r\\nПро це \\\"Українській правді\\\" повідомили в Генеральній прокуратурі.\\r\\nЗа словами співрозмовника, затримання провели працівники Генпрокуратури Молдови за запитом Генпрокуратури України, який ініціювало Національне антикорупційне бюро.\\r\\nУ свою чергу, заступник українського генпрокурора Євген Єнін повідомив, що молдавські правоохоронці затримали Чауса 28 лютого.\\r\\nДеталі він не став розкривати.\\r\\nЗа словами Єніна, попереду – екстрадиційна перевірка та видача судді Україні.\\r\\nУкраїнські правоохоронці тримають тісний контакт із партнерами.\\r\\nГПУ координуватиме цей процес.\\r\\nЯк відомо, 9 серпня 2016 року детективи Національного антикорупційного бюро викрили Чауса при отриманні хабара у 150 тисяч доларів, він ховав гроші в банці.\\r\\nУ вересні керівник Спеціалізованої антикорупційної прокуратури Назар Холодницький заявив, що Чаус перебуває в анексованому Росією Криму.\\r\\n11 листопада Інтерпол оголосив Чауса в міжнародний розшук.\\r\\n16 січня 2017 року Чаус не з\\\'явився на допит у НАБУ.\\r\\n10 лютого слідчий суддя Солом\\\'янського райсуду Києва повернув детективам Антикорупційного бюро клопотання про спеціальне досудове розслідування щодо Чауса через нібито недоведення міжнародного розшуку, однак НАБУ планувало повторно домагатися заочного розслідування цієї справи1', 1, '2017-03-01 13:24:00'),
(5, 'Суд дозволив заочне розслідування щодо Клименка', 'Голосіївський райсуд Києва дозволив заочне розслідування стосовно екс-міністра доходів і зборів часів Януковича Олександра Клименка.', 'Голосіївський райсуд Києва дозволив заочне розслідування стосовно екс-міністра доходів і зборів часів Януковича Олександра Клименка.\r\nПро це повідомили у Facebook заступник генпрокурора – головний військовий прокурор Анатолій Матіос та генпрокурор Юрій Луценко.\"Ми це зробили – \"заочка\" для Клименка є!  Честь справжньому українському суду і справедливим терезам Феміди в особі судді Мірошниченко!\" – написав Матіос.\r\n\"Після 17 годин \"процесуальних\" боїв та ігор адвокатів в хованки від суду, тільки що суддя Голосіївського суду Мирошниченко, розглянувши більше 30 клопотань захисту, надала дозвіл на проведення спеціального досудового розслідування (простіше – заочне розслідування) відносно Клименка О.В\", – повідомив він.', 1, '2017-03-01 13:23:04');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `username`, `password`) VALUES
(1, 'admin', 'admin', '$2y$10$sLOTeRLQ8bMhtJEqAbSLdO1lbqHaiw8lrWxk2O40ZyKpCkm548iqy');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
