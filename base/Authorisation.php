<?php
namespace base;

use models\User;

class Authorisation
{
    /**
     * @var array
     * user data
     */
    public $user;

    /**
     * @param $username
     * @param $password
     * @param $userModel
     * @return mixed
     * @throws \Exception
     */
    public function login($username, $password, $userModel)
    {
        $userData = $userModel->findByUsernamePassword($username, $password);
        if($userData){
            $userModel->user =  $userData;
            $_SESSION['user'] = $userModel->user;
            return  $_SESSION['user']['id'];
        }

        throw new \Exception('Invalid username password combination');
    }

    public function logout()
    {
        if(session_destroy()){
            return true;
        }
        return false;
    }

    public function isAuthorised()
    {
        return isset($_SESSION['user']);
    }
}