<?php
namespace base;

class DbConnection
{
    /**
     * @var \PDO $connection
     */
    private $connection;

    /**
     * DbConnection constructor.
     * @param $config
     */
    private function __construct($config)
    {
        $dsn = 'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'] . ';charset=utf8';
        $pdo = new \PDO($dsn, $config['user'], $config['password'], ['enableParamLogging'=>true,]);
        $this->connection = $pdo;
    }

    private function __clone() {}

    /**
     * @var DbConnection $instance
     */
    private static $instance;

    /**
     * @param $config
     * @return DbConnection
     */
    public static function setInstance($config)
    {
        if(self::$instance) {
            return self::$instance;
        }
        else {
            self::$instance = new self($config);
            return self::$instance;
        }
    }

    /**
     * @return \PDO
     * @throws \Exception
     */
    public function getConnection()
    {
        if($this->connection){
            return $this->connection;
        }

        throw new \Exception('PDO connection not set');
    }

}