<?php

namespace base;

class Request
{
    /**
     * @var string $controllerName
     */
    public $controllerName;

    /**
     * @var string $actionName
     */
    public $actionName;

    /**
     * @var string $data
     */
    public $data;

}