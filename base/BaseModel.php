<?php
namespace base;

abstract class BaseModel
{
    public $dbPrefix = 'anoxi_';
    /**
     * @return DbConnection
     */
    public function getDb()
    {
        return Application::getDb();
    }

    /**
     * @return \PDO
     */
    public function getConnection()
    {
        $db = $this->getDb();
        return $db->getConnection();
    }
}