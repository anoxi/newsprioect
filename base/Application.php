<?php
namespace base;

class Application
{
    /** @var  DbConnection */
    private static $db;

    /** @var string $controllerPath */
    public $controllerPath = 'controllers';

    /** @var  Request */
    private $request;

    /** @var  BaseController
     * */
    private $controller;

    /**
     * Application constructor.
     * @param $config
     */
    public function __construct($config)
    {
        if (isset($config['db'])) {
            $this->setDb(DbConnection::setInstance($config['db']));
        }
    }
    /**
     * @param $db DbConnection
     */
    public static function setDb($db)
    {
        self::$db = $db;
    }

    /**
     * @return DbConnection
     * @throws \Exception
     */
    public static function getDb()
    {
        if (self::$db) {
            return self::$db;
        }
        throw new \Exception('Db not set');
    }

    /**
     * @return Request
     * @throws \Exception
     */
    public function getRequest()
    {
        if (isset($this->request)) {
            return $this->request;
        }

        throw new \Exception('Request not set');
    }

    /**
     * @param Request $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }


    public function run()
    {
        $url = $_SERVER['REQUEST_URI'];
        $urlManager = new UrlManager();
        $this->request = $urlManager->parseUrl($url);
        $this->controller = $this->createController();
        $this->runAction($this->request->data);

    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private function createController()
    {
        $request = $this->getRequest();
        $className = $this->controllerPath . '\\' . ucfirst($request->controllerName) . 'Controller';
        if(class_exists($className)) {
            $controller = new $className;
            return $controller;
        }

        throw new \Exception('Can`n find controller');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function runAction()
    {
        $request = $this->getRequest();
        $actionName = $request->actionName ?? $this->controller->defaultAction;
        $actionName = 'action' . ucfirst($actionName);

        if(method_exists($this->controller, $actionName)){
            return $this->controller->$actionName($request->data);
        }

        throw new \Exception('Cant find action');
    }
}