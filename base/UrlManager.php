<?php

namespace base;

class UrlManager
{

    /**
     * @param $url
     * @return \base\Request
     */
    public function parseUrl(string $url)
    {
        $url = mb_strtolower($url);
        $url = preg_replace('/^(\/)/', '', $url);
        $url = preg_replace('/(\/)$/', '', $url);
        $data = explode('/', $url, 3);
        $request = new Request();

        if(isset($data[0]) && $data[0] !== ''){
            $request->controllerName = $data[0];
        }else{
            $request->controllerName = $this->getDefaultController();
        }

        $request->actionName = $data[1] ?? $this->getDefaultAction();
        $request->data = $data[2] ?? null;

        return $request;
    }


    public function getDefaultController()
    {
        return 'news';
    }

    public function getDefaultAction()
    {
        return 'index';
    }


}