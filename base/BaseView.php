<?php
namespace base;

class BaseView
{
    /**
     * Template being rendered.
     */
    protected $template = null;

    /**
     * @var string
     * extension template file
     */
    protected $tplExtension = '.php';

    /**
     * @var string
     * name of folder with templates
     *
     */
    protected $tplFolder = 'views';

    /**
     * Initialize a new view context.
     */
    public function __construct($template)
    {
        $this->template = $template;
    }

    /**
     * Render the template, returning it's content.
     * @param array $data Data made available to the view.
     * @return string The rendered template.
     */
    public function render(Array $data=[])
    {
        if($data !=[]){
            extract($data);
        }

        $path = BASE_DIR . $this->tplFolder . DS . $this->template . $this->tplExtension;
        ob_start();
        include($path);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}
