<?php
@session_start();
define('DS', DIRECTORY_SEPARATOR);
define('BASE_DIR', __DIR__ . DS);


spl_autoload_register(function ($class_name) {
    $file = BASE_DIR . str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});

$config = include_once('config.php');
$app = new \base\Application($config);

try {
    $app->run();
} catch (Exception $exception) {
    echo 'Can`t run application';
}

//debug function
function ddd($data, $die = true)
{
    echo '<pre>';
        var_dump($data);
    echo '</pre>';

   if($die){
       die();
   }
}