<!DOCTYPE html>
<html>
<head>
    <link type="text/css" rel="stylesheet" href="../../assets/css/user/login.css"  media="screen,projection"/>
    <title>Anoxi News/Login</title>
</head>
<body>
<script type="text/javascript" src="../../assets/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/user/login.js"></script>
<div class="login">
    <div class="login-triangle"></div>

    <h2 class="login-header">Log in</h2>

    <form class="login-container" action="login/" name="loginForm" method="post">
        <p>
            <input type="text" placeholder="login" required="required" id="login">
        </p>
        <p>
            <input type="password" placeholder="Password" required="required" id="password">
        </p>
        <p>
            <input type="submit" value="Log in" id="submitBtn">
        </p>
    </form>
</body>
</div>