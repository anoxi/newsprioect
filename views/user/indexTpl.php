<!DOCTYPE html>
<html>
<head>
    <link type="text/css" rel="stylesheet" href="../../assets/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../../assets/css/user/style.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Anoxi News/User</title>
</head>

<body>
<script type="text/javascript" src="../../assets/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/materialize.min.js"></script>
<script type="text/javascript" src="../../assets/js/user/main.js"></script>

<nav class="grey darken-4" role="navigation">
    <div class="nav-wrapper container">
        <span id="exit">Exit</span>
        <ul class="right hide-on-med-and-down">
            <li>
                <a target="_blank" href="//<?php echo $_SERVER['HTTP_HOST']; ?>">Go to site</a>
            </li>
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li>
                <a target="_blank" href="//<?php echo $_SERVER['HTTP_HOST']; ?>">Go to site</a>
            </li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>

<!-- Modal info -->
<div id="InfoModal" class="modal">
    <div class="modal-content">
        <h4>Info</h4>
        <p id="customModalInfo"></p>
    </div>
    <div class="modal-footer">
        <button class=" modal-action modal-close waves-effect waves-green btn-flat">Ok</button>
    </div>
</div>

<!-- Modal updateNews -->
<div id="update" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>Update News</h4>
        <form action="user/updateNews" method="post" name="updateNews">
            <div class="row">
                <input type="hidden" name="News[id]" id="updateId">
                <div class="input-field col s6">
                    <input type="text" class="validate" required="required" name="News[title]"  id="updateTitle">
                    <label for="title">Title</label>
                </div>
                <div class="input-field col s12">
                    <textarea  class="materialize-textarea" required="required" name="News[description]" id="updateDescription"></textarea>
                    <label for="textarea1">Description</label>
                </div>
                <div class="input-field col s12">
                    <textarea class="materialize-textarea" required="required" name="News[text]"  id="updateText"></textarea>
                    <label for="textarea1">Text</label>
                </div>
            </div>
            <button class="btn grey darken-4 updateNewsBtn" name="updateNews">Update</button>
        </form>
    </div>
</div>


<div id="addNewsBntCnt" class="center-align">
    <div>
        <h5 class="center-align">You can add record</h5>
    </div>
    <div class="container">
        <div class="row">
            <form action="user/createNews" method="post" name="createNews">
                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" class="validate" name="News[title]"  required="required">
                        <label for="title">Title</label>
                    </div>
                    <div class="input-field col s12">
                        <textarea class="materialize-textarea" required="required" name="News[description]"></textarea>
                        <label for="textarea1">Description</label>
                    </div>
                    <div class="input-field col s12">
                        <textarea class="materialize-textarea" required="required" name="News[text]"></textarea>
                        <label for="textarea1">Text</label>
                    </div>
                </div>
                <button class="btn grey darken-4 addNewsBtn" name="addNews">Add new</button>
            </form>
        </div>
        <hr><hr>
    </div>
</div>

<div class="container">
    <div class="section">
        <?php
            if(!empty($userData)){
                ?>
                    <div>
                       <h4 class="center-align">Entries that you added earlier</h4>
                    </div>
                   <hr>
                <?php
            }
        ?>
        <div class="row">
            <?php
                foreach($userData as $k => $v){
                    ?>
                    <div class="col-s12">
                        <h5 class="newsTitle">
                            <?php echo $v['title'];?>
                        </h5>
                        <div class="newsDescription">
                            <?php echo $v['description'];?>
                        </div>
                        <div class="newsText">
                            <?php echo $v['text'];?>
                        </div>
                        <div class="newsCreateTime grey-text">
                            Published: <?php echo $v['create_time'];?>
                        </div>
                        <div class="newsControlBtn" data-news-id="<?php echo $v['id'] ?>">
                            <button data-action="user/getNewsById" class="waves-effect waves-light btn grey darken-3">Update</button>
                            <button data-action="user/deleteNews" class="waves-effect waves-light btn grey darken-3">Delete</button>
                        </div>
                        <hr>
                    </div>
                    <?php
                }
            ?>
        </div>
    </div>
</body>
</html>