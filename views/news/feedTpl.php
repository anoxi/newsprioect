<?php
//ddd($news);
?>
<?php
//var_dump($news); die();
?>
<!DOCTYPE html>
<html>
<head>
    <link type="text/css" rel="stylesheet" href="../../assets/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../../assets/css/news/style.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Anoxi News</title>
</head>

<body>
<script type="text/javascript" src="../../assets/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/materialize.min.js"></script>
<script type="text/javascript" src="../../assets/js/news/main.js"></script>

<nav class="grey darken-4" role="navigation">
    <div class="nav-wrapper container">
        <a href="<?php echo '//'.$_SERVER['HTTP_HOST'];?>">
            <span id="exit">News</span>
        </a>
        <ul class="right hide-on-med-and-down">
            <li>
                <a href="user/login">Login</a>
            </li>
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li>
                <a href="user/login">Login</a>
            </li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>

<div class="container">
    <div class="section">
        <div class="row">
            <div class="col-s12">
                <h5 class="newsTitle">
                    <?php echo $news['title'];?>
                </h5>
                <div class="newsDescription">
                    <?php echo $news['text'];?>
                </div>
                <div class="newsCreateTime grey-text">
                    Published: <?php echo $news['create_time'];?>
                </div>
                <div class="newsControlBtn">
                    <a href="<?php echo '//'.$_SERVER['HTTP_HOST'];?>">
                        <button data-action="user/getNewsById" class="waves-effect waves-light btn grey darken-3">
                            Back
                        </button>
                    </a>
                </div>
                <hr>
            </div>
        </div>
    </div>
