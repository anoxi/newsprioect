<!DOCTYPE html>
<html>
<head>
    <link type="text/css" rel="stylesheet" href="../../assets/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../../assets/css/news/style.css"  media="screen,projection"/>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Anoxi News</title>
</head>

<body>
<script type="text/javascript" src="../../assets/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/materialize.min.js"></script>
<script type="text/javascript" src="../../assets/js/news/main.js"></script>

<nav class="grey darken-4" role="navigation">
    <div class="nav-wrapper container">
        <a href="<?php echo '//'.$_SERVER['HTTP_HOST'];?>">
            <span id="exit">News</span>
        </a>
        <ul class="right hide-on-med-and-down">
            <li>
                <a href="user/login">Login</a>
            </li>
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li>
                <a href="user/login">Login</a>
            </li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>

<div class="container">
    <div class="section">
        <div class="row">
            <?php
                if(empty($news)){
                    echo 'News not found';
                    exit;
                }

                foreach($news as $k => $v){
                    ?>
                    <div class="col-s12">
                        <h5 class="newsTitle">
                            <?php echo $v['title'];?>
                        </h5>
                        <div class="newsDescription">
                            <?php echo $v['description'];?>
                        </div>
                        <div class="newsCreateTime grey-text">
                            Published: <?php echo $v['create_time'];?>
                        </div>
                        <div class="newsControlBtn">
                            <a href="news/feed/id=<?php echo $v['id'];?>">
                                <button data-action="user/getNewsById" class="waves-effect waves-light btn grey darken-3">
                                    Read more
                                </button>
                            </a>
                        </div>
                        <hr>
                    </div>
                    <?php
                }
            ?>
        </div>
    </div>