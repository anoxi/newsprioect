<?php
namespace models;

use base\BaseModel;
use base\Authorisation;

class User extends BaseModel
{
    public $tabbleName = 'users';

    /**
     * @param $username
     * @param $password
     * @return bool|mixed
     */
    public function findByUsernamePassword($username, $password)
    {
        $connection = $this->getConnection();
        $statement = $connection->prepare("SELECT * FROM `users` WHERE `username` = '$username'");
        $statement->execute();
        try{
            $result = $statement->fetch(\PDO::FETCH_ASSOC);

            if(
                isset($result['password'])
                    &&
                password_verify( $password, $result['password'] )
            ){
                return $result;
            }
            return false;
        }catch (\Exception $exception){
            echo 'User not found';
        }
    }
}