<?php
namespace models;

use base\BaseModel;

class News extends BaseModel
{
    public $tabbleName = 'news';

    /**
     * @param array $data
     * @return bool
     */
    public function save(Array $data)
    {
        $connection = $this->getConnection();

        $statement = $connection->prepare(
            "INSERT INTO $this->tabbleName (title, description, text, author_id)VALUES(?,?,?,?)"
        );

        try{
            $result = $statement->execute(
                array(
                    $data['title'],
                    $data['description'],
                    $data['text'],
                    $data['author_id'],
                )
            );
            return $result;
        }catch (\Exception $exception){
            echo 'Can`t execute query';
        }
    }

    public function update(Array $data)
    {
        if($this->getNewsById($data['id'], $data['author_id'])){
            $connection = $this->getConnection();

            $sql = "UPDATE $this->tabbleName SET
            title = :title, 
            description = :description,  
            text = :text
            WHERE id = :newsId";

            $statement = $connection->prepare($sql);

            $statement->bindParam(':title', $data['title']);
            $statement->bindParam(':description', $data['description']);
            $statement->bindParam(':text', $data['text']);
            $statement->bindParam(':newsId', $data['id']);
            try{
                $result = $statement->execute();
                return $result;
            }catch (\Exception $exception){
                echo 'Can`t execute query';
            }

        }else{
            echo 'that`s not yuors news';
            return false;
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function getNewsByUId(int $id)
    {
        $connection = $this->getConnection();

        $statement = $connection->prepare("SELECT *  FROM $this->tabbleName WHERE author_id = $id ORDER BY create_time DESC");
        $statement->execute();

        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * @param $newsId
     * @param $authorId
     */
    public function deleteByUser($newsId, $authorId)
    {
        $connection = $this->getConnection();
        $statement = $connection->prepare("SELECT id  FROM $this->tabbleName WHERE author_id = $authorId AND id = $newsId");
        $statement->execute();

        $result = $statement->fetch();
        if($result){
            $statement = $connection->prepare("DELETE  FROM $this->tabbleName WHERE id = $newsId");
            return $statement->execute();
        }
        return false;
    }

    public function getNewsById($id, $authorId = false){
        $connection = $this->getConnection();

        $authorCOndition = '';

        if($authorId){
            $authorCOndition = 'AND author_id = '.$authorId;
        }

        $statement = $connection->prepare("SELECT *  FROM $this->tabbleName WHERE id = $id $authorCOndition");
        $statement->execute();

        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function getAllNews(){
        $connection = $this->getConnection();
        $statement = $connection->prepare("SELECT *  FROM $this->tabbleName");
        $statement->execute();

        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
}